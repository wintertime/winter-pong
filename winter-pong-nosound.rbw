#!/usr/bin/env ruby
# Copyright (c) 2013 Sebastian Zhorel

require_relative 'lib/winter-pong.rb'

#$windowsize = Vector2.new 1024,576
#$timestepsPerSec = 60
#$timestep = (1000000+$timestepsPerSec-1)/$timestepsPerSec

begin
  GameSounds.on false
  resX = 0
  resY = 0
  ARGV.each do |arg|
    argi = arg.to_i
    if argi != 0
      if resX == 0
        resX = argi
      elsif resY == 0
        resY = argi
      end
    elsif arg.match(/.*no.*sound.*/i) || arg.match(/.*sound.*off.*/i)
      GameSounds.on false
    elsif arg.match(/.*sound.*on.*/i)
      GameSounds.on true
    end
  end
  if resX != 0
    resX = 600 if resX < 600
    resY = 360 if resY < 360
    resY = (resX*9)/16 if resY < (resX*9)/16
    resX = (resY*16)/10 if resX < (resY*16)/10
    $windowsize = Vector2.new resX,resY
  end
  Game.new.run
end
