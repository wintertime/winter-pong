Welcome to Winter-Pong!
-----------------------

This game is made by Sebastian Zhorel aka wintertime
for the Power Up Table Tennis Contest at
http://www.gamedev.net/topic/647849-power-up-table-tennis-contest/ .
Please read the file licensing.txt for conditions of use!
I have to apologise in advance for any spelling or grammar mistakes you may
encounter, because I am not a native English speaker. Just notice, I
intentionally put an extraneous space in between URI or file names and
text punctuation to keep them separately for easier use.
Please do not hesitate to tell me about any remaining installation problems
--after reading the included install.txt file--, bugs or misspellings you may
encounter, to allow me to improve.
I would be happy to receive your feedback through the http://www.gamedev.net/
forums; in case that is too inconvenient for you, you can also email to
sebastian-zhorel <at> email.de to reach me. Please allow for some time passing
before a reply, for I can not afford to constantly check my mailbox.


Installation

This is only an abbreviation, install.txt contains the detailed instructions.

If you are on Windows (for other operating systems please read install.txt
instead), you need to have MRI Ruby 1.9.3 installed on your computer, which you
can download from http://rubyinstaller.org/ . Please use the installer and
select the available options to add Ruby to the path and associate the rb and
rbw file extensions with Ruby (you can also select the option to install TK).
Make sure you do not change the install directory to one including a space in
its name.
Installing Ruby will have added some items to the start menu including one
called "Start Command Prompt with Ruby" with the path set up for starting it.

If you would like to use Ruby for more than this game it is also recommended
for a complete Ruby installation (that is able to compile native gems) to
install the Ruby DevKit 4.5.2, which is available on the same page.
Additionally there is more documentation about installing this linked, that you
may want to read in that case, because you would have to type a few lines into
a console window for this. Alternatively look at install.txt , especially if
you have more than one version of Ruby installed.

The game package contains pre-compiled files for SFML and rbSFML to make
installation easier. These files depend on having the above-mentioned version
of Ruby; they may not work for other versions.
Note that the pre-compiled dll and gem files are only included for your
convenience and without any warranty, use them only at your own risk!
Getting the game installed is then only a matter of extracting the zip file at
an appropriate directory and double clicking install-rbSFML.bat once, to
install the needed rbSFML library through Ruby Gems into the Ruby installation.

Double clicking the file winter-pong.rbw will then start the game. There is
also winter-pong-nosound.rbw if you feel like playing in silence, or you can
add command-line arguments for "nosound" and/or numbers for choosing width and
height of the window. If started without sound the game will not load these
until you increase the sound volume.
There is a known bug of SFML-Audio, that may or may not show up on your system.
It is only happening when an application is shut down, because of a static
destruction issue of the AudioDevice and does not affect game-play in any way.
I can not do anything about it besides providing a means of deactivating sound,
but it is required for the contest. If you like to know more you can read it in
issue #30 at the SFML bug-tracker.

If you are having any doubts about correct installation, you are having any
problems with it, you want to try using a different Ruby implementation or you
are using an operating system different from Windows, please open the file
install.txt and read the more detailed installation instructions in there!


Usage

Winter-Pong can be fully operated with the keyboard, but it also supports using
the mouse for all menus. You can resize the game window if you like to.
Capitalized letters usually indicate keyboard commands in all screens. Inside
the main menu are also some operations which can be used through the
function keys.
The Escape key is working as a way to exit a game screen; other keys or
clicking with the mouse may also work, depending on which screen is shown at
that moment.
For example, the P key can be used to pause a running game.
A faster exit from the game than with Escape or clicking, that is, without
going to the main menu first, can be had using the usual OS command,
for example, clicking the close button or hitting Alt+F4 key combination. If a
game is played at that time you will also get one of two different confirmation
screens, depending on method used.
For playing, the red player on the left-hand side can use the W and S keys to
navigate his/her paddle up and down. After collecting a ball-time-freeze
power-up, he/she can use the D key to activate it and time-freeze the ball
for 3 seconds, in case the ball were likely to go outside the playing field.
For playing the blue player, the corresponding commands are the up arrow and
down arrow keys to control the paddle and the left arrow key to activate the
power-up. You have the choice of using the cursor keys or the numeric keypad.


Game Description

I made this game for a contest on http://www.gamedev.net/ , which got inspired
by the classic Pong games. I can not remember ever playing those myself, and I
assume Winter-Pong is largely different than those, since it contains many more
features to meet the requirements of the contest.

When you start the game you will see the intro screen. Use any key or click
after the loading bar filled and vanished to get into the main menu. Keyboard
shortcuts are displayed at left side of this screen.
Before starting a play session from the main menu, you are able to choose from
some options to customize the game.
For any of the two players you can choose whether you would like that side to
be controlled by keyboard or an AI. F1 and F2 keys are the shortcuts for this.
There are also options for adjusting the strength of the AI, which lets it
control its paddle better, and starting difficulty level of the game, which
will increase the ball speed at higher levels. The shortcuts for these are
R, T, Y/Z, U, I, O and D, F, G, H, J, K.
Adjusting the maximum score to be reached for winning is also possible. The
keyboard shortcuts are X, C, V, B, N, M.
Last button row is for adjusting sound volume. For this task you can use the
keys End, Page Down, Down Arrow, Up Arrow, Page Up and Home; both cursor keys
and numeric keypad will do. If you started the game without sounds they get
loaded when increasing sound volume from 0.
Press the S key or click Start now to begin a game; press Q or click Quit to
close the game.

The paddles are rounded and allow for controlling the direction of the ball
by having it hit a differently curved part. This also means it requires a bit
of skill, to not let the ball bounce outwards over the edge of the paddle.

You can see the score from the numbers in the top-left and top-right corner in
red and blue colour, corresponding to the two players. Every time a player lets
the ball outside the playing field on his/her side, the opposite player will
gain a point. First player reaching a score of 10 points wins.
In the top middle is a number indicating the current difficulty level.
The level will start at the difficulty you set before the game. During play it
will increase and in these moments the background changes to remove all of the
snow covering the power-up items.
A timer showing the length of the current game is positioned at the bottom.

Power-up items spawn randomly on the playing field and can be collected by
directing the ball into them. All power-up items, with the exception of the
ball-time-freeze, are activated automatically on the time of impact.
The speed-up power-up is represented by a red clock icon and doubles the speed,
with which your own paddle moves. The paddle will then be shown blurry and with
a small red number of the remaining time.
The speed-down power-up is represented by a blue clock icon and halves the
speed, with which the paddle of the enemy player moves. The paddle will show
this through a snow covering making its colour lighter and a small blue number
showing the remaining time.
The ball-time-freeze power-up is represented by a light-blue snowflake icon and
is the only one that can be collected for later use. You will see how many you
have available from the light-blue number on your side of the playing field
(just below your score), indicating if you could use one on a key press.
There is another power-up represented by a yellow candle light. This power-up
will increase the breadth of your paddle for a time that is displayed in orange
in the middle of your paddle.
Last power-up is a cyan ice cube. When getting one it reduces the breadth of
the opponents paddle and shows the time duration in cyan.

After finishing read it is time to start a game now.


Have fun playing!
