# Copyright (c) 2013 Sebastian Zhorel

require 'sfml/graphics'
include SFML

$windowsize = Vector2.new 1024,576
$timestepsPerSec = 60
$timestep = (1000000+$timestepsPerSec-1)/$timestepsPerSec

$textColor = Color::Black
$textBold = true

require_relative 'winter-pong/gamesounds'

require_relative 'winter-pong/gameobject'
require_relative 'winter-pong/ball'
require_relative 'winter-pong/paddle'
require_relative 'winter-pong/powerup'
require_relative 'winter-pong/ballfreeze'
require_relative 'winter-pong/snowsystem'

require_relative 'winter-pong/bounds'
require_relative 'winter-pong/paddlemovement'
require_relative 'winter-pong/player'
require_relative 'winter-pong/keyboardplayer'
require_relative 'winter-pong/aiplayer'

require_relative 'winter-pong/gametimer'
require_relative 'winter-pong/score'
require_relative 'winter-pong/difficulty'

require_relative 'winter-pong/guibutton'
require_relative 'winter-pong/guidoublebutton'
require_relative 'winter-pong/guibuttonrow'

def createText string,color,position
  text = Text.new
  text.setFont $font
  text.setCharacterSize $windowsize.y/8
  if $textBold
    bold = 1 # Text::Bold missing in rbSFML
    text.setStyle bold
  end
  text.setColor color
  text.setString string
  bounds = text.getGlobalBounds
  text.setPosition $windowsize*position-0.5*Vector2.new(bounds.width,bounds.height)
  text
end

def event_loop window
  while window.open?
    window.each_event do |event|
      if event.type == Event::Closed
        return nil
      else
        return false unless yield event
      end
    end
    window.clear Color::White
    return false unless yield nil
    window.display
  end
  nil
end

def wait_event_loop window
  while true
    window.clear Color::White
    return false unless yield nil
    window.display
    break if !window.open?
    event = window.wait_event
    begin
      if event.type == Event::Closed
        return nil
      else
        return false unless yield event
      end
    end while event = window.poll_event
  end
  nil
end

def screen_loop window
  wait_event_loop window do |event|
    unless event.nil?
      if event.type == Event::KeyReleased || event.type == Event::MouseButtonReleased
        false
      else
        yield event
      end
    else
      yield nil
    end
  end
end

class Game
  def initialize
    vmode = VideoMode.getDesktopMode
    if vmode.width < $windowsize.x || vmode.height < $windowsize.y
      $windowsize.x = vmode.width
      $windowsize.y = vmode.height-64
      $windowsize.x = ($windowsize.y*16)/9 if $windowsize.y < ($windowsize.x*9)/16
      $windowsize.y = ($windowsize.x*10)/16 if $windowsize.x < ($windowsize.y*16)/10
    end
    @window = RenderWindow.new([$windowsize.x,$windowsize.y],'winter-pong')
    @window.clear Color.new(191,191,191,255)
    @window.display
    #@window.vertical_sync_enabled = true

    @menuBGTexture = Texture.new
    @menuBGTexture.loadFromFile 'datamod/pass_in_snow2.jpg'
    @menuBGTexture.smooth = true
    @menuBG = RectangleShape.new $windowsize
    @menuBG.setTexture @menuBGTexture
    @window.clear Color::White
    @window.draw @menuBG
    @window.display

    @useAI = [0,1]

    $font = Font.new
    unless ['font/*.{ttf,TTF}','font/*','*.{ttf,TTF}','**/*.{ttf,TTF}','*','**/*'].detect { |pattern|
      fontFilenames = Dir.glob pattern
      fontFilenames.detect { |fn| $font.loadFromFile(fn) }
    }
      raise 'could not find any font file'
    end
    @introtexts = [
      createText('wintertime',$textColor,Vector2.new(0.5,0.125)),
      createText('presents',$textColor,Vector2.new(0.5,0.25)),
      createText('Winter-Pong',Color.new(63,127,255,255),Vector2.new(0.5,0.375)),
      createText('A Game By',$textColor,Vector2.new(0.5,0.625)),
      createText('Sebastian Zhorel',$textColor,Vector2.new(0.5,0.75))
    ]
    @introtexts[1].setCharacterSize $windowsize.y/16
    bounds = @introtexts[1].getGlobalBounds
    @introtexts[1].setPosition $windowsize*Vector2.new(0.5,0.25)-0.5*Vector2.new(bounds.width,bounds.height)
    @introtexts[2].setCharacterSize $windowsize.y/4
    bounds = @introtexts[2].getGlobalBounds
    @introtexts[2].setPosition $windowsize*Vector2.new(0.5,0.375)-0.5*Vector2.new(bounds.width,bounds.height)
    @introtexts[3].setCharacterSize $windowsize.y/16
    bounds = @introtexts[3].getGlobalBounds
    @introtexts[3].setPosition $windowsize*Vector2.new(0.5,2.0/3.0)-0.5*Vector2.new(bounds.width,bounds.height)
    bounds = @introtexts[4].getGlobalBounds
    @introtexts[4].setPosition $windowsize*Vector2.new(0.5,0.75)-0.5*Vector2.new(bounds.width,bounds.height)
    @pausetext = createText 'Pause...',$textColor,Vector2.new(0.5,0.5)
  end

  def loading
    barMaxSize = ($windowsize.x*30)/32
    barSize = Vector2.new 1,$windowsize.y/16
    bar = RectangleShape.new barSize
    bar.setPosition $windowsize.x/32,($windowsize.y*14)/16
    bar.setFillColor Color.new(0,0,255,127)
    maxCounter = 18+1
    counter = 0
    event_loop @window do |event|
      unless event.nil?
        true
      else
        case counter
        when 1
          @sound = GameSounds.new
          @sound.load :menus,'sound/wind5.ogg',true
          @sound.play :menus
        when 2
          @sound.load :pausing,'sound/atmosbasement.ogg',true
        when 3
          @sound.load :playing,'sound/GalacticTemple.ogg',true
        when 4
          @sound.load :guihover,'sound/beep.ogg',false
        when 5
          @sound.load :guidown,'sound/on.ogg',false
        when 6
          @sound.load :guiup,'sound/off.ogg',false
          GUIButton.setSound @sound
        when 7
          @sound.load :failing,'sound/xylophone-1.ogg',false
        when 8
          @sound.load :winning,'sound/xylophone-2.ogg',false
        when 9
          @sound.load :bounce1,'sound/button1.ogg',false
        when 10
          @sound.load :bounce2,'sound/button2.ogg',false
        when 11
          @sound.load :levelup,'sound/alarm_0.ogg',false
        when 12
          @sound.load :powerup,'sound/getruby.ogg',false
        when 13
          @sound.load :freezing,'sound/complete.ogg',false
          GameSounds.setLoaded
        when 14
          @guiTexture = Texture.new
          @guiTexture.loadFromFile 'data/gui-elements.png'
          @guiTexture.smooth = true
        when 15
          Ball.load
        when 16
          Paddle.load
        when 17
          Powerup.load
        when 18
          SnowSystem.load
        end
        counter += 1
        barSize.x = (barMaxSize*counter)/maxCounter
        bar.setSize barSize
        @window.draw @menuBG
        @introtexts.each { |text| @window.draw text }
        @window.draw bar
        counter <= maxCounter
      end
    end
  end

  def intro
    screen_loop @window do |event|
      unless event.nil?
      else
        @window.draw @menuBG
        @introtexts.each { |text| @window.draw text }
      end
      true
    end
  end

  def mainmenu
    textRed = createText 'Red',Color::Red,Vector2.new(1.0/4.0,0.5/16.0)
    textBlue = createText 'Blue',Color::Blue,Vector2.new(3.0/4.0,0.5/16.0)
    buttonAI = [
      GUIDoubleButton.new(@guiTexture,Vector2.new(0.5/16.0,1.5/16.0),@useAI[0],['keyboard','AI'],'F1',[Keyboard::F1]),
      GUIDoubleButton.new(@guiTexture,Vector2.new((8.0+0.5)/16.0,1.5/16.0),@useAI[1],['keyboard','AI'],'F2',[Keyboard::F2])
    ]
    buttonAIStrength = GUIButtonRow.new(@guiTexture,Vector2.new(0.5/16.0,3.5/16.0),Bounds.smoothing,0,10,2,'AI strength:','RTYUIO',
      [[Keyboard::R],[Keyboard::T],[Keyboard::Y,Keyboard::Z],[Keyboard::U],[Keyboard::I],[Keyboard::O]])
    buttonDifficulty = GUIButtonRow.new(@guiTexture,Vector2.new(0.5/16.0,5.5/16.0),Difficulty.startlevel,0,100,10,'difficulty:','DFGHJK',
      [[Keyboard::D],[Keyboard::F],[Keyboard::G],[Keyboard::H],[Keyboard::J],[Keyboard::K]])
    buttonMaxScore = GUIButtonRow.new(@guiTexture,Vector2.new(0.5/16.0,7.5/16.0),Score.maxvalue,1,1000,10,'max. score:','XCVBNM',
      [[Keyboard::X],[Keyboard::C],[Keyboard::V],[Keyboard::B],[Keyboard::N],[Keyboard::M]])
    buttonVolume = GUIButtonRow.new(@guiTexture,Vector2.new(0.5/16.0,9.5/16.0),@sound.getVolume,0,100,10,'sounds:','Down/Up',
      [[Keyboard::End,Keyboard::Numpad1],[Keyboard::PageDown,Keyboard::Numpad3],[Keyboard::Down,Keyboard::Numpad2],
        [Keyboard::Up,Keyboard::Numpad8],[Keyboard::PageUp,Keyboard::Numpad9],[Keyboard::Home,Keyboard::Numpad7]])
    text = createText 'Start',$textColor,Vector2.new(0.5,12.0/16.0)
    buttonStart = GUIButton.new text,[Keyboard::S]
    text = createText 'Quit',$textColor,Vector2.new(0.5,14.0/16.0)
    buttonQuit = GUIButton.new text,[Keyboard::Q,Keyboard::Escape]
    resultStart = false
    resultQuit = false
    resultLoad = false
    result = wait_event_loop @window do |event|
      unless event.nil?
        (0..1).each { |id| @useAI[id] = buttonAI[id].input event,@window }
        Bounds.smoothing = buttonAIStrength.input event,@window
        Difficulty.startlevel = buttonDifficulty.input event,@window
        Score.maxvalue = buttonMaxScore.input event,@window
        volume = buttonVolume.input event,@window
        @sound.setVolume(volume)
        resultLoad = !GameSounds.isLoaded && volume != 0
        resultStart = buttonStart.input event,@window
        resultQuit = buttonQuit.input event,@window
        !(resultStart||resultQuit||resultLoad)
      else
        @window.draw @menuBG
        @window.draw textRed
        @window.draw textBlue
        buttonAI.each { |button| button.draw @window }
        buttonAIStrength.draw @window
        buttonDifficulty.draw @window
        buttonMaxScore.draw @window
        buttonVolume.draw @window
        buttonStart.draw @window
        buttonQuit.draw @window
        true
      end
    end
    return nil if result.nil? || resultQuit
    return 1 if resultLoad
    resultStart
  end

  def confirm message
    text = createText 'Yes',$textColor,Vector2.new(1.0/3.0,2.0/3.0)
    buttonYes = GUIButton.new text,[Keyboard::Y]
    text = createText 'No',$textColor,Vector2.new(2.0/3.0,2.0/3.0)
    buttonNo = GUIButton.new text,[Keyboard::N]
    text = createText message,$textColor,Vector2.new(0.5,1.0/3.0)
    resultYes = false
    resultNo = false
    result2 = wait_event_loop @window do |event|
      unless event.nil?
        resultYes = buttonYes.input event,@window
        resultNo = buttonNo.input event,@window
        !(resultYes || resultNo || (event.type == Event::KeyReleased && event.key.code != Keyboard::F4 && event.key.code != Keyboard::LAlt && event.key.code != Keyboard::RAlt))
      else
        @window.draw @menuBG
        @window.draw text
        buttonYes.draw @window
        buttonNo.draw @window
        true
      end
    end
    return nil if result2.nil?
    resultYes
  end

  def reset
    @scores = [Score.new(0),Score.new(1)]
    @powerups = [Powerup.new(0),Powerup.new(1),Powerup.new(2),Powerup.new(3),Powerup.new(4)]
    textureFreeze = Powerup.texture 2
    @ballFreeze = [BallFreeze.new(0,textureFreeze),BallFreeze.new(1,textureFreeze)]
    @ball = Ball.new @ballFreeze
    @snowSystem = SnowSystem.new
    @paddleMovements = [PaddleMovement.new,PaddleMovement.new]
    @paddles = [Paddle.new(0),Paddle.new(1)]
    @players = [nil,nil]
    if @useAI[0]==0
      @players[0] = KeyboardPlayer.new(@paddleMovements[0],@ballFreeze[0],[Keyboard::W],[Keyboard::S],[Keyboard::D])
    else
      @players[0] = AIPlayer.new(@paddleMovements[0],@ballFreeze[0],Bounds.new(@paddles[0]),Bounds.new(@ball))
    end
    if @useAI[1]==0
      @players[1] = KeyboardPlayer.new(@paddleMovements[1],@ballFreeze[1],[Keyboard::Up,Keyboard::Numpad8],[Keyboard::Down,Keyboard::Numpad2],[Keyboard::Left,Keyboard::Numpad4])
    else
      @players[1] = AIPlayer.new(@paddleMovements[1],@ballFreeze[1],Bounds.new(@paddles[1]),Bounds.new(@ball))
    end
    @timer = GameTimer.new
    @difficulty = Difficulty.new @timer,@scores
  end

  def play
    @sound.play :playing
    reset
    pause = false
    tstepsum = 0
    tsteps = 0
    @timer.endpause
    while true
      result = event_loop @window do |event|
        unless event.nil?
          if event.type == Event::MouseButtonReleased
            false
          elsif event.type == Event::KeyReleased && event.key.code == Keyboard::Escape
            false
          elsif event.type == Event::KeyReleased && event.key.code == Keyboard::P
            if !pause
              @timer.startpause
              @sound.pause :playing
              @sound.play :pausing
              pause = true
            else
              @sound.stop :pausing
              @sound.play :playing
              @timer.endpause
              pause = false
            end
            true
          else
            if !pause
              substeptime = (@timer.gametime?.asMicroseconds-tstepsum).to_f / $timestep.to_f
              @players.each { |player| player.input event,substeptime }
            end
            true
          end
        else
          @timer.nextframe
          gametime = @timer.gametime?
          if tstepsum + $timestep < gametime.asMicroseconds
            while tstepsum + $timestep < gametime.asMicroseconds
              tstepsum += $timestep
              tsteps += 1
              @players.each { |player| player.update 1.0 }
              (0..1).each { |playerid| @paddles[playerid].update(@paddleMovements[playerid].get) }
              case @ball.update(@difficulty.level,@paddles[0].getBounds,@paddles[1].getBounds)
              when 0
                @sound.play :failing
                @scores[1].addPoint
              when 1
                @sound.play :failing
                @scores[0].addPoint
              when -1
                @sound.play :bounce1
              when -2
                @sound.play :bounce2
              end
              ballBounds = @ball.getBounds
              @powerups.each do |powerup|
                case powerup.update(ballBounds)
                when 0
                  @sound.play :powerup
                  case powerup.id
                  when 0
                    @paddles[0].speedup
                  when 1
                    @paddles[1].speeddown
                  when 2
                    @ballFreeze[0].add
                  when 3
                    @paddles[0].enlarge
                  when 4
                    @paddles[1].reduce
                  end
                when 1
                  @sound.play :powerup
                  case powerup.id
                  when 0
                    @paddles[1].speedup
                  when 1
                    @paddles[0].speeddown
                  when 2
                    @ballFreeze[1].add
                  when 3
                    @paddles[1].enlarge
                  when 4
                    @paddles[0].reduce
                  end
                end
              end
              if @difficulty.update
                @sound.play :levelup
                @snowSystem.reset
              end
              @ballFreeze.each do |freeze|
                if freeze.used
                  @sound.play :freezing
                end
              end
              @snowSystem.update
            end
            @paddleMovements.each { |pm| pm.reset }
          end
          @window.draw @menuBG
          @powerups.each { |powerup| powerup.draw @window }
          @snowSystem.draw @window
          @paddles.each { |paddle| paddle.draw @window }
          @ball.draw @window
          @scores.each { |score| score.draw @window }
          @difficulty.draw @window
          @timer.draw @window
          if pause
            @window.draw @pausetext
          end
          !@scores[0].won? && !@scores[1].won?
        end
      end
      if !pause
        @timer.startpause
        @sound.pause :playing
        @sound.play :pausing
      end
      if result.nil?
        if confirm('Exit to OS now?')!=false
          @window.close
          @sound.stop :pausing
          return nil
        end
      elsif @scores[0].won? || @scores[1].won?
        @sound.stop :pausing
        break
      else
        case confirm 'Quit this game?'
        when nil
          @window.close
          @sound.stop :pausing
          return nil
        when true
          @sound.stop :pausing
          break
        end
      end
      if !pause
        @sound.stop :pausing
        @sound.play :playing
        @timer.endpause
      end
    end
    @sound.stop :playing
    #p(@timer.gametime?.asMicroseconds/@timer.frame)
    result
  end

  def showscore
    @sound.stop :levelup
    if @scores[0].won?
      text = createText 'Red is winning!',Color::Red,Vector2.new(0.5,0.5)
      @sound.stop :failing
      @sound.play :winning
    elsif @scores[1].won?
      text = createText 'Blue is winning!',Color::Blue,Vector2.new(0.5,0.5)
      @sound.stop :failing
      @sound.play :winning
    else
      text = createText 'You quit - nobody is winning. T.T',$textColor,Vector2.new(0.5,0.5)
      @sound.play :failing
    end
    wait_event_loop @window do |event|
      unless event.nil?
        if event.type == Event::MouseButtonReleased
          false
        elsif event.type == Event::KeyReleased && (event.key.code == Keyboard::Escape || event.key.code == Keyboard::Return || event.key.code == Keyboard::Space || event.key.code == Keyboard::Pause)
          false
        else
          true
        end
      else
        @window.draw @menuBG
        @scores.each { |score| score.draw @window }
        @difficulty.draw @window
        @timer.draw @window
        @window.draw text
        true
      end
    end
  end

  def run
    if loading.nil?
      @sound.stopall
      return
    end
    if intro.nil?
      @sound.stopall
      return
    end
    while true
      case mainmenu
      when nil
        @sound.stopall
        return
      when 1
        if loading.nil?
          @sound.stopall
          return
        end
        redo
      end
      @sound.stop :menus
      if play.nil?
        @sound.stopall
        return
      end
      @sound.play :menus
      if showscore.nil?
        @sound.stopall
        return
      end
    end
  end
end
