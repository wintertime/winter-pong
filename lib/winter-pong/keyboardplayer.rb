# Copyright (c) 2013 Sebastian Zhorel

class KeyboardPlayer < Player
  def initialize paddleMovement,ballFreeze,keysUp,keysDown,keysFreeze
    super paddleMovement,ballFreeze
    @keysUp = keysUp
    @keysDown = keysDown
    @keysFreeze = keysFreeze
    @keypressFreeze = false
  end
  def input event,time
    if event.type == Event::KeyPressed
      @keysUp.find { |code| code==event.key.code ? @paddleMovement.up(time) : false }
      @keysDown.find { |code| code==event.key.code ? @paddleMovement.down(time) : false }
      @keysFreeze.find do |code|
        if code==event.key.code && !@keypressFreeze
          @keypressFreeze=true
          @ballFreeze.use
        else
          false
        end
      end
    elsif event.type == Event::KeyReleased
      @keysUp.find { |code| code==event.key.code ? @paddleMovement.stop(time) : false }
      @keysDown.find { |code| code==event.key.code ? @paddleMovement.stop(time) : false }
      @keysFreeze.find do |code|
        if code==event.key.code
          @keypressFreeze = false
          true
        else
          false
        end
      end
    end
  end
  def update time
    @keysUp.each { |code| @paddleMovement.up time if Keyboard::isKeyPressed(code) }
    @keysDown.each { |code| @paddleMovement.down time if Keyboard::isKeyPressed(code) }
  end
end
