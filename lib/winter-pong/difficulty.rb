# Copyright (c) 2013 Sebastian Zhorel

class Difficulty
  @@startlevel = 10
  def self.startlevel= level
    @@startlevel = level
  end
  def self.startlevel
    @@startlevel
  end
  attr_reader :level
  def initialize timer,scores
    @timer = timer
    @scores = scores
    @level = @@startlevel
    @text = createText @level.to_s,$textColor,Vector2.new(0.5,0.0625)
    bold = 1 # Text::Bold missing in rbSFML
    @text.setStyle bold
    @text.setPosition(($windowsize.x-@text.getGlobalBounds.width)/2,$windowsize.y/64)
  end
  def update
    level = @@startlevel+(@scores[0].getPoints+@scores[1].getPoints+@timer.gametime?.asSeconds.to_i/60)/3
    if level != @level
      @level = level
      @text.setString @level.to_s
      @text.setPosition(($windowsize.x-@text.getGlobalBounds.width)/2,$windowsize.y/64)
      true
    else
      false
    end
  end
  def draw window
    window.draw @text
  end
end
