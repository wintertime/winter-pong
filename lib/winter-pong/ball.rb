# Copyright (c) 2013 Sebastian Zhorel

class Ball < GameObject
  def self.load
    @@texture = [Texture.new,Texture.new]
    @@texture[0].loadFromFile 'data/ball.png'
    @@texture[1].loadFromFile 'data/ball-shadow.png'
    @@texture[0].smooth = true
    @@texture[1].smooth = true
  end
  def initialize ballFreeze
    @ballFreeze = ballFreeze
    @size = Vector2.new $windowsize.x/64,$windowsize.x/64
    @sizeshadow = (@size*90)/64
    @dispshadow = Vector2.new @size.x-@sizeshadow.x,@sizeshadow.y-@size.y-$windowsize.x/256
    @movespeed = ($windowsize.x+$windowsize.y)/8
    @shape = [RectangleShape.new(@size),RectangleShape.new(@sizeshadow)]
    @shape[0].setTexture @@texture[0]
    @shape[1].setTexture @@texture[1]
    reset
  end
  def reset
    temp = 0.25 + rand
    if temp >= 0.75
      temp += 0.5
    end
    temp *= Math::PI
    @forward = Vector2.new(Math::sin(temp),Math::cos(temp))
    @shape[0].setPosition(($windowsize-@size)/2)
    @shape[1].setPosition(($windowsize-@size)/2 + @dispshadow)
    @resetdelay = $timestepsPerSec
  end
  def update level,bounds0,bounds1
    if @resetdelay != 0
      @resetdelay -= 1
      return nil
    end
    if @ballFreeze[0].update
      return nil
    end
    if @ballFreeze[1].update
      return nil
    end
    result = nil
    currentspeed = @movespeed*(1.0+level/20.0)
    movement = (@forward*currentspeed*$timestep)/1000000.0
    nextposition = @shape[0].getPosition + movement
    # check wall collision
    if nextposition.y <= 0.0
      nextposition.y = -nextposition.y
      @forward.y = -@forward.y
      arc = Math::asin @forward.x
      if arc<0.0
        arc = Math::PI*-0.5-(Math::PI*-0.5-arc)*0.875
      else
        arc = Math::PI*0.5-(Math::PI*0.5-arc)*0.875
      end
      @forward.x = Math::sin arc
      @forward.y = Math::cos arc
      result = -2
    elsif nextposition.y >= $windowsize.y-@size.y
      nextposition.y = 2.0*($windowsize.y-@size.y)-nextposition.y
      @forward.y = -@forward.y
      arc = Math::asin @forward.x
      if arc<0.0
        arc = Math::PI*-0.5-(Math::PI*-0.5-arc)*0.875
      else
        arc = Math::PI*0.5-(Math::PI*0.5-arc)*0.875
      end
      @forward.x = Math::sin arc
      @forward.y = -(Math::cos arc)
      result = -2
    end
    # check paddle collision
    if nextposition.x <= bounds0.left+bounds0.width
      if (nextposition.y+@size.y >= bounds0.top) && (nextposition.y <= bounds0.top+bounds0.height)
        reflectCenter = Vector2.new bounds0.left+bounds0.width-bounds0.height,bounds0.top+0.5*bounds0.height
        contactBallCenterX = bounds0.left+bounds0.width+0.5*@size.x
        contactMovementFraction = ((nextposition.x+0.5*@size.x)-contactBallCenterX)/movement.x
        contactBallCenter = Vector2.new contactBallCenterX, (nextposition.y+0.5*@size.y)-movement.y*(1.0-contactMovementFraction)
        reflectNormal = contactBallCenter-reflectCenter
        reflectNormal /= Math::sqrt(reflectNormal.x*reflectNormal.x+reflectNormal.y*reflectNormal.y)
        movement *= 1.0-contactMovementFraction
        movement += (-2.0*(movement.x*reflectNormal.x+movement.y*reflectNormal.y))*reflectNormal
        @forward += (-2.0*(@forward.x*reflectNormal.x+@forward.y*reflectNormal.y))*reflectNormal
        nextposition = (contactBallCenter+movement)-0.5*@size
        result = -1
      end
    elsif nextposition.x+@size.x >= bounds1.left
      if (nextposition.y+@size.y >= bounds1.top) && (nextposition.y <= bounds1.top+bounds1.height)
        reflectCenter = Vector2.new bounds1.left+bounds1.height,bounds1.top+0.5*bounds1.height
        contactBallCenterX = bounds1.left-0.5*@size.x
        contactMovementFraction = ((nextposition.x+0.5*@size.x)-contactBallCenterX)/movement.x
        contactBallCenter = Vector2.new contactBallCenterX, (nextposition.y+0.5*@size.y)-movement.y*(1.0-contactMovementFraction)
        reflectNormal = contactBallCenter-reflectCenter
        reflectNormal /= Math::sqrt(reflectNormal.x*reflectNormal.x+reflectNormal.y*reflectNormal.y)
        movement *= 1.0-contactMovementFraction
        movement += (-2.0*(movement.x*reflectNormal.x+movement.y*reflectNormal.y))*reflectNormal
        @forward += (-2.0*(@forward.x*reflectNormal.x+@forward.y*reflectNormal.y))*reflectNormal
        nextposition = (contactBallCenter+movement)-0.5*@size
        result = -1
      end
    end
    # check out of bounds
    if nextposition.x < 0.0
      reset
      return 0
    elsif nextposition.x >= $windowsize.x
      reset
      return 1
    end
    @shape[0].setPosition nextposition
    @shape[1].setPosition nextposition+@dispshadow
    result
  end
  def draw window
    window.draw @shape[1]
    window.draw @shape[0]
    if @resetdelay == 0
      if !(@ballFreeze[0].draw window,getBounds)
        @ballFreeze[1].draw window,getBounds
      end
    end
    @ballFreeze.each { |freeze| freeze.draw2 window }
  end
end
