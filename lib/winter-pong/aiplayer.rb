# Copyright (c) 2013 Sebastian Zhorel

class AIPlayer < Player
  def initialize paddleMovement,ballFreeze,paddleBounds,ballBounds
    super paddleMovement,ballFreeze
    @paddleBounds = paddleBounds
    @ballBounds = ballBounds
    @paddleRect = paddleBounds.get
    @ballRect = ballBounds.get
    @distanceX = ((@paddleRect.left+0.5*@paddleRect.width) - (@ballRect.left+0.5*@ballRect.width)).abs
    @usedFreeze = false
  end
  def update time
    paddleRect = @paddleBounds.get
    ballRect = @ballBounds.get
    distanceX = ((paddleRect.left+0.5*paddleRect.width) - (ballRect.left+0.5*ballRect.width)).abs
    if distanceX <= @distanceX
      if distanceX < 0.5*$windowsize.x
        oldballY = @ballRect.top+@ballRect.height/2
        ballY = ballRect.top+ballRect.height/2
        if paddleRect.top+(paddleRect.height*3)/4 < ballY*2-oldballY
          @paddleMovement.down time
          if !@usedFreeze && paddleRect.top+paddleRect.height < ballY && distanceX < 0.25*$windowsize.x && (ballRect.top-(paddleRect.top+paddleRect.height))*$windowsize.x > $windowsize.y*distanceX
            if @ballFreeze.use
              @usedFreeze = true
            end
          end
        elsif paddleRect.top+(paddleRect.height*1)/4 > ballY*2-oldballY
          @paddleMovement.up time
          if !@usedFreeze && paddleRect.top > ballY && distanceX < 0.25*$windowsize.x && ((ballRect.top+ballRect.height)-paddleRect.top)*$windowsize.x > $windowsize.y*distanceX
            if @ballFreeze.use
              @usedFreeze = true
            end
          end
        end
      else
        @usedFreeze = false
      end
    end
    @paddleRect = paddleRect
    @ballRect = ballRect
    @distanceX = distanceX
  end
end
