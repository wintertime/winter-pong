# Copyright (c) 2013 Sebastian Zhorel

# ensure fairness by restricting access for AI
class Bounds
  @@smoothing = 2
  def self.smoothing= value
    @@smoothing = value
  end
  def self.smoothing
    @@smoothing
  end
  def initialize gameobject
    @gameobject = gameobject
    @randY = 0.0
  end
  def get
    bounds = @gameobject.getBounds.clone
    randYNew = rand($windowsize.y/4)-$windowsize.y/8
    @randY = (@@smoothing*@randY+2*randYNew)/(@@smoothing+2.0)
    bounds.top += @randY
    bounds.top=0 if bounds.top<0
    bounds.top=$windowsize.y-bounds.height if bounds.top>$windowsize.y-bounds.height
    bounds
  end
end
