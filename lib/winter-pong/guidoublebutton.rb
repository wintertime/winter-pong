# Copyright (c) 2013 Sebastian Zhorel

class GUIDoubleButton
  def initialize guiTexture,position,status,stringsButton,stringKey,keys
    @texture = guiTexture
    @position = position
    @textKey = createText stringKey,$textColor,position
    @text = [
      createText(stringsButton[0],$textColor,Vector2.new(position.x+2.5/16.0,position.y)),
      createText(stringsButton[1],$textColor,Vector2.new(position.x+4.5/16.0,position.y))
    ]
    @shape = [
      RectangleShape.new(Vector2.new($windowsize.x/32.0,$windowsize.x/32.0)),
      RectangleShape.new(Vector2.new($windowsize.x/32.0,$windowsize.x/32.0))
    ]
    @textKey.setPosition($windowsize.x*position.x,$windowsize.y*position.y)
    (0..1).each do |id|
      @text[id].setPosition($windowsize.x*(position.x+(2.5+4*id)/16.0),$windowsize.y*(position.y+0.0/16.0))
      @shape[id].setTexture @texture
      @shape[id].setPosition($windowsize.x*(position.x+(1.5+4*id)/16.0),$windowsize.y*(position.y+1.0/16.0))
    end
    setStatus status
    @button = [
      GUIButton.new(@shape[0],keys),
      GUIButton.new(@shape[1],keys)
    ]
  end
  def setStatus status
    @status = status
    (0..1).each do |id|
      @shape[id].setTextureRect Rect.new(128*(id*@status+(1-id)*(1-@status)),128,64,64)
    end
  end
  def getStatus
    @status
  end
  def input event,window
    click = false
    @button.each do |button|
      if button.input(event,window)
        click = true
      end
    end
    if click
      setStatus(1-@status)
    end
    @status
  end
  def draw window
    window.draw @textKey
    @text.each { |text| window.draw text }
    @button.each { |button| button.draw window }
  end
end
