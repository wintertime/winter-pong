# Copyright (c) 2013 Sebastian Zhorel

class GameObject
  def getBounds
    @shape[0].getGlobalBounds
  end
end
