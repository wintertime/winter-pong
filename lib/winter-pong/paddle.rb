# Copyright (c) 2013 Sebastian Zhorel

class Paddle < GameObject
  # id=0 is left, id=1 is right
  def self.load
    @@texture = [Texture.new,Texture.new,Texture.new,Texture.new]
    @@texture[0].loadFromFile 'data/paddle-red.png'
    @@texture[1].loadFromFile 'data/paddle-blue.png'
    @@texture[2].loadFromFile 'data/paddle-lightred.png'
    @@texture[3].loadFromFile 'data/paddle-lightblue.png'
    @@texture.each { |texture| texture.smooth = true }
  end
  def initialize id
    if id!=0 && id!=1
      raise ArgumentError
    end
    @id = id
    @sizes = [
      Vector2.new($windowsize.x/64,$windowsize.y/8),
      Vector2.new($windowsize.x/64,($windowsize.y*5)/32),
      Vector2.new($windowsize.x/64,($windowsize.y*3)/32)
    ]
    @size = @sizes[0]
    @sizediff = [@sizes[1].y-@sizes[0].y,@sizes[0].y-@sizes[2].y]
    @movespeed = ($windowsize.y-@size.y)*2
    @shape = [RectangleShape.new(@size)]
    @position = Vector2.new(id*($windowsize.x-@size.x),($windowsize.y-@size.y)/2)
    @oldposition = @position
    @shape[0].setPosition @position
    @shape[0].setTexture @@texture[id]
    @speedup = 0
    @speeddown = 0
    @enlarge = 0
    @reduce = 0
    @text = [
      createText('0',Color.new(255,127,63,255),Vector2.new(0.5+3.0/128.0,0.5)),
      createText('0',Color.new(63,127,255,255),Vector2.new(0.5+3.0/128.0,0.5)),
      createText('0',Color.new(255,191,0,255),Vector2.new(0.5+3.0/128.0,0.5)),
      createText('0',Color.new(0,191,255,255),Vector2.new(0.5+3.0/128.0,0.5))
    ]
    bold = 1 # Text::Bold missing in rbSFML
    @text.each do |text|
      text.setCharacterSize $windowsize.y/32
      text.setStyle bold
    end
  end
  def speedup
    @speedup += $timestepsPerSec*30
  end
  def speeddown
    @speeddown += $timestepsPerSec*30
  end
  def enlarge
    if @enlarge == 0
      if @reduce == 0
        @size = @sizes[1]
        @oldposition.y -= @sizediff[0]/2
        @position.y -= @sizediff[0]/2
      else
        @size = @sizes[0]
        @oldposition.y -= @sizediff[1]/2
        @position.y -= @sizediff[1]/2
      end
      if @position.y < 0
        @position.y = 0
      end
      if @position.y > $windowsize.y-@size.y
        @position.y = $windowsize.y-@size.y
      end
      @shape[0].setPosition @position
      @shape[0].setSize @size
    end
    @enlarge += $timestepsPerSec*30
  end
  def reduce
    if @reduce == 0
      if @enlarge == 0
        @size = @sizes[2]
        @oldposition.y += @sizediff[1]/2
        @position.y += @sizediff[1]/2
      else
        @size = @sizes[0]
        @oldposition.y += @sizediff[0]/2
        @position.y += @sizediff[0]/2
      end
      @shape[0].setPosition @position
      @shape[0].setSize @size
    end
    @reduce += $timestepsPerSec*30
  end
  def update paddleMovement
    position = @position.clone
    movement = (paddleMovement*@movespeed*$timestep)/1000000.0
    if @speedup != 0
      @speedup -= 1
      movement *= 2
    end
    if @speeddown != 0
      @speeddown -= 1
      movement /= 2
    end
    if @enlarge != 0
      @enlarge -= 1
      if @enlarge == 0
        if @reduce == 0
          @size = @sizes[0]
          position.y += @sizediff[0]/2
          @position.y += @sizediff[0]/2
        else
          @size = @sizes[2]
          position.y += @sizediff[1]/2
          @position.y += @sizediff[1]/2
        end
        @shape[0].setPosition position
        @shape[0].setSize @size
      end
    end
    if @reduce != 0
      @reduce -= 1
      if @reduce == 0
        if @enlarge == 0
          @size = @sizes[0]
          position.y -= @sizediff[1]/2
          @position.y -= @sizediff[1]/2
        else
          @size = @sizes[1]
          position.y -= @sizediff[0]/2
          @position.y -= @sizediff[0]/2
        end
        if @position.y < 0
          @position.y = 0
        end
        if @position.y > $windowsize.y-@size.y
          @position.y = $windowsize.y-@size.y
        end
        if position.y < 0
          position.y = 0
        end
        if position.y > $windowsize.y-@size.y
          position.y = $windowsize.y-@size.y
        end
        @shape[0].setPosition position
        @shape[0].setSize @size
      end
    end
    position.y -= movement
    if position.y < 0
      position.y = 0
    end
    if position.y > $windowsize.y-@size.y
      position.y = $windowsize.y-@size.y
    end
    if position.y != @position.y
      @oldposition = @position
      @position = position
      @shape[0].setPosition position
    end
  end
  def draw window
    @shape[0].setTexture @@texture[@id + (@speeddown==0 ? 0 : 2)]
    if @speedup == 0
      @shape[0].setFillColor Color::White
    else
      @shape[0].setFillColor Color.new(255,255,255,63)
      if @oldposition.y < @position.y
        @shape[0].setPosition @position.x,@position.y-$windowsize.y/128-1
      else
        @shape[0].setPosition @position.x,@position.y+$windowsize.y/128+1
      end
      window.draw @shape[0]
      @shape[0].setFillColor Color.new(255,255,255,127)
      @shape[0].setPosition @position
    end
    window.draw @shape[0]
    if @speedup != 0
      seconds = @speedup/$timestepsPerSec
      @text[0].setString seconds.to_s
      @text[0].setPosition(@position.x+(1-@id*1.2)*@size.x*1.25-@id*@text[0].getGlobalBounds.width,@position.y)
      window.draw @text[0]
    end
    if @speeddown != 0
      seconds = @speeddown/$timestepsPerSec
      @text[1].setString seconds.to_s
      @text[1].setPosition(@position.x+(1-@id*1.2)*@size.x*1.25-@id*@text[1].getGlobalBounds.width,@position.y+@size.y-$windowsize.y/32)
      window.draw @text[1]
    end
    if @enlarge != 0
      seconds = @enlarge/$timestepsPerSec
      @text[2].setString seconds.to_s
      @text[2].setPosition(@position.x+(1-@id*1.3)*@size.x*1.5-@id*@text[2].getGlobalBounds.width,@position.y + (@reduce != 0 ? (@size.y-$windowsize.y/32)/3 : (@size.y-$windowsize.y/32)/2))
      window.draw @text[2]
    end
    if @reduce != 0
      seconds = @reduce/$timestepsPerSec
      @text[3].setString seconds.to_s
      @text[3].setPosition(@position.x+(1-@id*1.3)*@size.x*1.5-@id*@text[3].getGlobalBounds.width,@position.y + (@enlarge != 0 ? ((@size.y-$windowsize.y/32)*2)/3 : (@size.y-$windowsize.y/32)/2))
      window.draw @text[3]
    end
  end
end
