# Copyright (c) 2013 Sebastian Zhorel

class GUIButtonRow
  def initialize guiTexture,position,value,valueMin,valueMax,valueFastStep,stringDescription,stringKey,keys
    @texture = guiTexture
    @position = position
    @value = value
    @valueMin = valueMin
    @valueMax = valueMax
    @valueFastStep = valueFastStep
    @textKey = createText stringKey,$textColor,position
    @text = [
      createText(stringDescription,$textColor,Vector2.new(position.x+6.5/16.0,position.y)),
      createText(value.to_s,$textColor,Vector2.new(position.x+11.0/16.0,position.y)),
    ]
    @shape = [
      RectangleShape.new(Vector2.new($windowsize.x/32.0,$windowsize.x/32.0)),
      RectangleShape.new(Vector2.new($windowsize.x/32.0,$windowsize.x/32.0)),
      RectangleShape.new(Vector2.new($windowsize.x/32.0,$windowsize.x/32.0)),
      RectangleShape.new(Vector2.new($windowsize.x/32.0,$windowsize.x/32.0)),
      RectangleShape.new(Vector2.new($windowsize.x/32.0,$windowsize.x/32.0)),
      RectangleShape.new(Vector2.new($windowsize.x/32.0,$windowsize.x/32.0))
    ]
    @textKey.setPosition($windowsize.x*position.x,$windowsize.y*position.y)
    @text[0].setPosition($windowsize.x*(position.x+6.5/16.0),$windowsize.y*(position.y+0.0/16.0))
    @text[1].setPosition($windowsize.x*(position.x+11.0/16.0)-@text[1].getGlobalBounds.width,$windowsize.y*(position.y+0.0/16.0))
    (0..5).each do |id|
      @shape[id].setTexture @texture
    end
    (0..2).each do |id|
      @shape[id].setTextureRect Rect.new(64*id,64,64,64)
      @shape[id].setPosition($windowsize.x*(position.x+(3.5+id)/16.0),$windowsize.y*(position.y+1.0/16.0))
    end
    (3..5).each do |id|
      @shape[id].setTextureRect Rect.new(64*(id-3),0,64,64)
      @shape[id].setPosition($windowsize.x*(position.x+(3.5+5.0+id)/16.0),$windowsize.y*(position.y+1.0/16.0))
    end
    @button = [
      GUIButton.new(@shape[0],keys[0]),
      GUIButton.new(@shape[1],keys[1]),
      GUIButton.new(@shape[2],keys[2]),
      GUIButton.new(@shape[3],keys[3]),
      GUIButton.new(@shape[4],keys[4]),
      GUIButton.new(@shape[5],keys[5])
    ]
  end
  def input event,window
    click = [false,false,false,false,false,false]
    (0..5).each do |id|
      if @button[id].input(event,window)
        click[id] = true
      end
    end
    @value = @valueMin if click[0]
    @value = @valueMax if click[5]
    @value -= @valueFastStep if click[1]
    @value += @valueFastStep if click[4]
    @value -= 1 if click[2]
    @value += 1 if click[3]
    @value = @valueMin if @value < @valueMin
    @value = @valueMax if @value > @valueMax
    @text[1].setString @value.to_s
    @text[1].setPosition($windowsize.x*(@position.x+11.0/16.0)-@text[1].getGlobalBounds.width,$windowsize.y*(@position.y+0.0/16.0))
    @value
  end
  def draw window
    window.draw @textKey
    @text.each { |text| window.draw text }
    @button.each { |button| button.draw window }
  end
end
