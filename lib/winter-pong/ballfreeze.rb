# Copyright (c) 2013 Sebastian Zhorel

class BallFreeze
  def initialize id,texture
    @id = id
    @available = 0
    @active = 0
    @used = false
    @texture = texture
    @size = Vector2.new(($windowsize.x*3)/128,($windowsize.x*3)/128)
    @shape = RectangleShape.new @size
    @shape.setTexture @texture
    color = Color.new(63,127,255,255)
    @text = createText '0',color,Vector2.new(0.5+3.0/128.0,0.5)
    @text.setCharacterSize $windowsize.y/32
    bold = 1 # Text::Bold missing in rbSFML
    @text.setStyle bold
    @text2 = createText '0',color,Vector2.new(1.0/64.0,(62*id+1)/64.0)
    @text2.setStyle bold
  end
  def add
    @available += 1
  end
  def use
    if @available == 0
      false
    else
      @available -= 1
      @used = true
      @active += $timestepsPerSec*3
      true
    end
  end
  def used
    temp = @used
    @used = false
    temp
  end
  def update
    if @active != 0
      @active -= 1
      true
    else
      false
    end
  end
  def draw window,ballRect
    if @active != 0
      @shape.setPosition(ballRect.left+(ballRect.width-@size.x)/2,ballRect.top+(ballRect.height-@size.y)/2)
      window.draw @shape
      seconds = @active/$timestepsPerSec
      @text.setString seconds.to_s
      @text.setPosition(ballRect.left+ballRect.width,ballRect.top+ballRect.height)
      window.draw @text
      true
    else
      false
    end
  end
  def draw2 window
    @text2.setString @available.to_s
    if @id==0
      @text2.setPosition($windowsize.x/32,($windowsize.y*9)/64)
    else
      @text2.setPosition(($windowsize.x*31)/32-@text2.getGlobalBounds.width,($windowsize.y*9)/64)
    end
    window.draw @text2
  end
end
