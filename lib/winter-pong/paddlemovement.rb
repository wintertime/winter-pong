# Copyright (c) 2013 Sebastian Zhorel

class PaddleMovement
  def initialize
    @moveacceleration = 1.0/16.0
    @movespeed = 0.0
    @nextmove = 0.0
    reset
  end
  def reset
    @time = 0.0
  end
  def up time
    if @movespeed < 0.0
      @movespeed = @moveacceleration*(time-@time)
    else
      @movespeed += @moveacceleration*(time-@time)
      if @movespeed > 1.0
        @movespeed = 1.0
      end
    end
    @nextmove += @movespeed*(time-@time)
    if @nextmove > 1.0
      @nextmove = 1.0
    end
    @time = time
    #p "up"
  end
  def down time
    if @movespeed > 0.0
      @movespeed = -@moveacceleration*(time-@time)
    else
      @movespeed -= @moveacceleration*(time-@time)
      if @movespeed < -1.0
        @movespeed = -1.0
      end
    end
    @nextmove += @movespeed*(time-@time)
    if @nextmove < -1.0
      @nextmove = -1.0
    end
    @time = time
    #p "down"
  end
  def stop time
    @movespeed = 0.0
    @time = time
  end
  def get
    tmp = @nextmove
    @nextmove = 0.0
    tmp
  end
end
