# Copyright (c) 2013 Sebastian Zhorel

class SnowSystem
  @@numMax = 600
  def self.load
    @@texture = [Texture.new]
    @@texture[0].loadFromFile 'data/snow.png'
    @@texture.each { |texture| texture.smooth = true }
  end
  def initialize
    @rs = RenderStates.new @@texture[0]
    reset
  end
  def reset
    @num = 0
    @va = VertexArray.new Triangles,0
    @va2 = VertexArray.new Triangles,0
  end
  def update
    if @num < @@numMax
      pos = Vector2.new(rand($windowsize.x),rand($windowsize.y))
      posdiff = $windowsize/64
      @va.append Vertex.new(pos+Vector2.new(-posdiff.x,-posdiff.y),Vector2.new(0.0,128.0))
      @va.append Vertex.new(pos+Vector2.new(posdiff.x,-posdiff.y),Vector2.new(128.0,128.0))
      @va.append Vertex.new(pos+Vector2.new(0,posdiff.y),Vector2.new(64.0,0.0))
      @num += 1
    else
      @va2 = @va
      @va = VertexArray.new Triangles,0
      @num = 0
    end
  end
  def draw window
    window.draw @va2,@rs
    window.draw @va,@rs
  end
end
