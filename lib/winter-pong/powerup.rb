# Copyright (c) 2013 Sebastian Zhorel

class Powerup < GameObject
  @@chancedivider = 30
  def self.load
    @@texture = [Texture.new,Texture.new,Texture.new,Texture.new,Texture.new]
    @@texture[0].loadFromFile 'datamod/wall_clock-red.png'
    @@texture[1].loadFromFile 'datamod/wall_clock-blue.png'
    @@texture[2].loadFromFile 'data/freezing-snowflake.png'
    @@texture[3].loadFromFile 'data/candle.png'
    @@texture[4].loadFromFile 'data/ice-cubic.png'
    @@texture.each { |texture| texture.smooth = true }
  end
  def self.texture id
    @@texture[id]
  end
  attr_reader :id
  def initialize id
    if id!=0 && id!=1 && id!=2 && id!=3 && id!=4
      raise ArgumentError
    end
    @id = id
    @size = Vector2.new $windowsize.x/32,$windowsize.x/32
    @shape = [RectangleShape.new(@size)]
    @shape[0].setTexture @@texture[id]
    @status = 0
  end
  def update ballRect
    result = nil
    case @status
    when 0
      if 1.0 >= @@chancedivider*$timestepsPerSec*rand
        # put it on field
        @shape[0].setPosition(($windowsize.x/32+(($windowsize.x*30)/32-@size.x)*rand),(($windowsize.y-@size.y)*rand))
        @status = 1
      end
    when 1
      # check hit
      position = @shape[0].getPosition+0.5*@size
      ballposition = Vector2.new ballRect.left+0.5*ballRect.width,ballRect.top+0.5*ballRect.height
      diff = ballposition-position
      diff2 = diff*diff
      dist2 = diff2.x+diff2.y
      radius = (ballRect.width+ballRect.height+@size.x+@size.y)/4.0
      if dist2 <= radius*radius
        @status = 0
        if @ballRect.left < ballRect.left
          result = 0
        else
          result = 1
        end
      end
    end
    @ballRect = ballRect
    result
  end
  def draw window
    if @status == 1
      window.draw @shape[0]
    end
  end
end
