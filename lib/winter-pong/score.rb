# Copyright (c) 2013 Sebastian Zhorel

class Score
  @@maxvalue = 10
  def self.maxvalue= value
    @@maxvalue = value
  end
  def self.maxvalue
    @@maxvalue
  end
  def initialize playerid
    reset
    @playerid = playerid
    case playerid
    when 0
      @text = createText @value.to_s,Color::Red,Vector2.new(0.0625,0.0625)
    when 1
      @text = createText @value.to_s,Color::Blue,Vector2.new(0.9375,0.0625)
    else
      raise ArgumentError
    end
    bold = 1 # Text::Bold missing in rbSFML
    @text.setStyle bold
  end
  def reset
    @value = 0
  end
  def addPoint
    @value += 1
  end
  def won?
    @value == @@maxvalue
  end
  def getPoints
    @value
  end
  def draw window
    @text.setString @value.to_s
    if @playerid==0
      @text.setPosition($windowsize.x/32,$windowsize.y/64)
    else
      @text.setPosition(($windowsize.x*31)/32-@text.getGlobalBounds.width,$windowsize.y/64)
    end
    window.draw @text
  end
end
