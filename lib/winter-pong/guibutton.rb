# Copyright (c) 2013 Sebastian Zhorel

class GUIButton
  def self.setSound sound
    @@sound = sound
  end
  def initialize shape,keys
    @keys = keys
    @activekey = nil
    @hover = false
    @active = false
    @bounds = shape.getGlobalBounds
    @shape = [shape,RectangleShape.new(Vector2.new(@bounds.width,@bounds.height))]
    @shape[1].setPosition @bounds.left,@bounds.top
  end
  def input event,window
    result = false
    if event.type == Event::MouseMoved
      mxy = window.mapPixelToCoords(Vector2.new(event.mouseMove.x,event.mouseMove.y))
      hover = @bounds.contains?(mxy)
      if hover != @hover
        @hover = hover
        @@sound.play :guihover
      end
    elsif event.type == Event::MouseButtonPressed
      mxy = window.mapPixelToCoords(Vector2.new(event.mouseButton.x,event.mouseButton.y))
      if @bounds.contains?(mxy)
        if !@active
          @active = true
          @@sound.play :guidown
        end
      end
    elsif event.type == Event::MouseButtonReleased
      if @active
        @active = false
        mxy = window.mapPixelToCoords(Vector2.new(event.mouseButton.x,event.mouseButton.y))
        if @bounds.contains?(mxy)
          @@sound.play :guiup
          result = true
        else
          @@sound.play :guidown
        end
      end
    elsif event.type == Event::KeyPressed
      if @keys.find { |code| code==event.key.code ? @activekey=code : false }
        @@sound.play :guidown
      end
    elsif event.type == Event::KeyReleased
      @keys.find do |code|
        if code==event.key.code
          if @activekey==code
            @activekey = nil
            @@sound.play :guiup
            result = true
          end
          true
        else
          false
        end
      end
    end
    result
  end
  def draw window
    window.draw @shape[0]
    if @hover
      @shape[1].setFillColor Color.new(255,255,255,127)
      window.draw @shape[1]
    end
    if @active
      @shape[1].setFillColor Color.new(255,0,0,127)
      window.draw @shape[1]
    end
    if @activekey
      @shape[1].setFillColor Color.new(0,0,255,127)
      window.draw @shape[1]
    end
  end
end
