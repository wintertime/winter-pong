# Copyright (c) 2013 Sebastian Zhorel

class GameTimer
  attr_reader :frame
  def initialize
    @clock = Clock.new
    @starttime = @clock.getElapsedTime
    @framestarttime = @starttime
    @pause = @starttime
    @pausetime = microseconds 0
    @frame = 0
    @text = createText '0:00',$textColor,Vector2.new(0.5,0.8125)
  end
  def nextframe
    if !@pause
      @frame += 1
      @framestarttime = @clock.getElapsedTime
    else
      @framestarttime + (@clock.getElapsedTime-@pause)
    end
  end
  def startpause
    if !@pause
      @pause = @clock.getElapsedTime
    end
  end
  def endpause
    if @pause
      pauselength = @clock.getElapsedTime-@pause
      @framestarttime += pauselength
      @pausetime += pauselength
      @pause = nil
    end
  end
  def gametime?
    if !@pause
      @clock.getElapsedTime - (@starttime+@pausetime)
    else
      @pause - (@starttime+@pausetime)
    end
  end
  def frametime?
    if !@pause
      @clock.getElapsedTime-@framestarttime
    else
      @pause-@framestarttime
    end
  end
  def draw window
    seconds = gametime?.asSeconds.to_i
    minutes = seconds/60
    seconds -= minutes*60
    @text.setString minutes.to_s+(seconds<10 ? ':0' : ':')+seconds.to_s
    @text.setPosition(($windowsize.x-@text.getGlobalBounds.width)/2,($windowsize.y*55)/64)
    window.draw @text
  end
end
