# Copyright (c) 2013 Sebastian Zhorel

require 'sfml/audio'
include SFML

class GameSounds
  @@volume = 50
  @@on = true
  @@loaded = false
  def self.on yesno
    @@on = yesno
    @@volume = 0 unless yesno
  end
  def self.on?
    @@on
  end
  def self.setLoaded
    @@loaded = true if @@on
  end
  def self.isLoaded
    @@loaded
  end
  def initialize
    @buffers = Hash.new
    @sounds = Hash.new
  end
  def stopall
    @sounds.each { |symbol,sound| sound.stop }
  end
  def getVolume
    @@volume
  end
  def setVolume volume
    @@volume = volume
    @@on = (volume != 0)
    @sounds.each { |symbol,sound| sound.setVolume volume }
  end
  def load symbol,filename,loop
    if !@@loaded
      if !@@on
        return false
      end
      buf = SoundBuffer.new
      if !buf.loadFromFile(filename)
        return false
      end
      @buffers[symbol] = buf
      sound = Sound.new buf
      sound.setLoop loop
      sound.setVolume(@@volume)
      @sounds[symbol] = sound
    end
    true
  end
  def play symbol
    if !@@on
      stopall
      return false
    end
    sound = @sounds[symbol]
    if sound
      sound.play
      true
    else
      false
    end
  end
  def pause symbol
    if !@@on
      stopall
      return false
    end
    sound = @sounds[symbol]
    if sound
      sound.pause
      true
    else
      false
    end
  end
  def stop symbol
    if !@@on
      stopall
      return false
    end
    sound = @sounds[symbol]
    if sound
      sound.stop
      true
    else
      false
    end
  end
end
